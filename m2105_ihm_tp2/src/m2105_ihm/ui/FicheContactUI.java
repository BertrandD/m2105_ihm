/*
 * Module 2105 : module IHM : Carnet d'adresse
 */
package m2105_ihm.ui;

import java.awt.GridLayout;
import java.util.HashMap;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import m2105_ihm.nf.*;

/**
 *
 * @author IUT2
 */
public class FicheContactUI extends JPanel {

    private JTextField       champNom;
    JComboBox dispos,region;
    JTextField champDate,autre,prenom,email,numTel;
    HashMap<String,JCheckBox> listeHobbies;
    JCheckBox autreCB;
    HashMap<String,JCheckBox> listeMusique;
    
    
            
    /**
     * Formulaire pour saisir les informations relatives à un contact
     */
    public FicheContactUI() {
        initUIComponents();
    }
    
    /**
     * Crée et positionne les composants graphiques constituant l'interface
     */
    private void initUIComponents() {      
        /*
         * Ajoute dans l'IHM un champ pour la saisie/Affichage du nom
         */
      
        this.setLayout(new GridLayout(9,2));
        champNom = new JTextField(30);
        dispos = new JComboBox(DispoSortie.values());
        champDate = new JTextField(10);
        listeHobbies = new HashMap<String,JCheckBox>();
        for(Hobby h:Hobby.values()){
            listeHobbies.put(h.getLabel(),new JCheckBox(h.getLabel()));
        }
        autreCB = new JCheckBox("Autre ");
        autre = new JTextField(10);
        prenom = new JTextField(10);
        email = new JTextField(15);
        numTel = new JTextField(10);
        listeMusique = new HashMap<String,JCheckBox>();
        for(StyleMusical s:StyleMusical.values()){
            listeMusique.put(s.getLabel(),new JCheckBox(s.getLabel()));
        }
        region = new JComboBox(Region.values());
               
        
        this.add(new JLabel("Nom :"));
        this.add(champNom);
        
        this.add(new JLabel("Dispos :"));
        this.add(dispos);
        
        this.add(new JLabel("Date :"));
        this.add(champDate);
        
        this.add(new JLabel("Hobbies :"));
        
        JPanel colHobby = new JPanel();
        colHobby.setLayout(new GridLayout(5,1));
        for(JCheckBox j:listeHobbies.values()){
            colHobby.add(j);
        }

        JPanel colAutre = new JPanel();
        colAutre.add(autreCB);
        colAutre.add(autre);

        colHobby.add(colAutre);
        
        this.add(colHobby);
        
        this.add(new JLabel("Prénom :"));
        this.add(prenom);
        
        this.add(new JLabel("Email :"));
        this.add(email);
        
        this.add(new JLabel("Numéro de téléphone :"));
        this.add(numTel);
        
        this.add(new JLabel("Styles musicaux :"));

        JPanel col4 = new JPanel();
        col4.setLayout(new GridLayout(listeMusique.size()/3,3));
        for(JCheckBox j:listeMusique.values()){
            col4.add(j);
        }
        this.add(col4);

        this.add(new JLabel("Région :"));
        this.add(region);       

         
    }
    
    /**
     * Affecte des valeurs au formulaire de fiche contact
     * @param contact un contact
     * @return
     */
    public boolean setValues(Contact contact) {
        boolean success = false;
        
        if (contact != null) {
            /*
             * Nom du contact
             */
            champNom.setText(contact.getNom());
            dispos.setSelectedItem(contact.getDisponibilite());
            champDate.setText(
                    contact.getDateNaissance()[2]+"/"+
                    contact.getDateNaissance()[1]+"/"+
                    contact.getDateNaissance()[0]);
            for(Hobby h:contact.getHobbies()){
                listeHobbies.get(h.getLabel()).setSelected(true);
            }
            if(contact.getHobbyAutre().isEmpty()){
                autreCB.setSelected(true);
                autre.setText(contact.getHobbyAutre());
            }
            prenom.setText(contact.getPrenom());
            email.setText(contact.getEmail());
            numTel.setText(contact.getNumeroTelephone());
            for(StyleMusical h:contact.getGoutsMusicaux()){
                listeMusique.get(h.getLabel()).setSelected(true);
            }
            region.setSelectedItem(contact.getRegion());
             /*
             * A COMPLETER ...
             */
            
            success = true;
        }
        
        return success;
    }
    
    /**
     * Retourne les valeurs associées au formulaire de fiche contact
     * @return
     */
    public boolean getValues(Contact contact) {        
        /*
         * Affecte une valeur à l'attribut Nom avec le nom saisi dans le champ
         * correspondant de l'IHM
         */
        contact.setNom(champNom.getText());
      
        /*
         * A COMPLETER ...
         */
        
        return true;
    }
}

/*
 * Module 2105 : module IHM : Carnet d'adresse
 */
package m2105_ihm;

/**
 *
 * @author IUT2
 */
import m2105_ihm.nf.*;
import m2105_ihm.ui.*;
import java.awt.Point;
import javax.swing.JFrame;


public class Main {
    
    public static void main(String[] args) { 
        /*
         * Exemple de création d'un groupe
         */
        Contact contact = new Contact();
        contact.setNom("Dupont");
        contact.setPrenom("Bob");
        contact.setDateNaissance(1995, 06, 11);
        contact.setNumeroTelephone("06.19.06.19.06");
        contact.setHobbyAutre("Swing");
        contact.setRegion(Region.RHONE_ALPES);
        contact.setDisponibilite(DispoSortie.WEEK_END);
        contact.addStyle(StyleMusical.RNB);
        contact.addStyle(StyleMusical.MONDE);
        contact.addHobby(Hobby.CINEMA);
        contact.addHobby(Hobby.LECTURE);
        contact.setHobbyAutre("Informatique");
        
        /*
         * Exemple de création d'un groupe
         */
        GroupeContacts groupe;        
        Point [] points = new Point[4];
        
        points[0] = new Point(20,20);
        points[1] = new Point(100,20);
        points[2] = new Point(100,100);
        points[3] = new Point(20,100);
        
        groupe = new GroupeContacts();
        groupe.setNom("Copains IUT2");
        groupe.setPoints(points);

        /*
         * Exemple de création du carnet
         */
        CarnetPlanning carnet;
        
        carnet = new CarnetPlanning();
        carnet.ajouterContact(contact);
        carnet.ajouterGroupe(groupe);
        carnet.ajouterContactGroupe(groupe, contact); // association contact -> groupe
        
        /*
         * Création d'une fenêtre (Objet JFrame)
         */
        JFrame window = new JFrame();
        window.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        window.setSize(1000, 800);
        
        /*
         * Ajout de l'IHM de la fiche groupe de contact vierge dans le JFrame
         */
        FicheGroupeUI ficheGroupe = new FicheGroupeUI();        
        ficheGroupe.setValues(groupe, carnet.listerContactsGroupe(groupe));
        ficheGroupe.getValues(groupe);
        
        /*
         * Affiche dans la console les champs modifiés dans l'IHM
         */
        ExempleNF.exempleAfficherCarnetPlanning(carnet);

        /*
         * Ajoute la fiche groupe de contacts dans la fenêtre
         */
        window.add(ficheGroupe);

        
        /*
         * Rendre visible la fenêtre
         */
        window.setVisible(true);
    }
}

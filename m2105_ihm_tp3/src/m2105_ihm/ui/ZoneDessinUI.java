/*
 * Module 2105 : module IHM : Carnet d'adresse
 */
package m2105_ihm.ui;

import java.awt.Point;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Polygon;
import java.awt.Graphics;
import java.awt.Dimension;
import static java.awt.PageAttributes.ColorType.COLOR;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *
 * @author IUT2
 */
/**
 * 
 * @class ZoneDessinUI
 * Zone d'édition d'un logo
 */
public class ZoneDessinUI extends Canvas implements MouseListener {

    /*
     * Liste des points définissant le dessin
     */
    private Polygon polygon = new Polygon();
    
    public ZoneDessinUI() {        
        /*
         * A COMPLETER POUR LA GESTION D'EVENEMENTS SOURIS
         */
        this.addMouseListener(this);
        
    }
    
    /**
     * Dessine le contenu du canvas, c'est-à-dire l'icone
     * @param g un contexte graphique
     */
    public void paint(Graphics g) {
        /*
         * A MODIFIER et COMPLETER
         */
       Dimension dim = this.getSize();
       this.setSize(dim);
      
        /*
         * Dessine une diagonale en fonction de la taille du canvas
         */
        this.setBackground(Color.white);
        g.setColor(Color.blue);
        g.drawRect(0,0,this.getWidth()-1,this.getHeight()-1);
        
        g.setColor(Color.red);
        g.drawPolygon(getPolygon());

        //g.fillRect(0,0,dim.width,dim.height);
        
        
        this.setVisible(true);
        
        //g.drawLine(0, 0, dim.width, dim.height);
        
    }

    /**
     * Efface le dessin
     */
    public void effacer() {
        getPolygon().reset();
        this.repaint();
        /*
         * A COMPLETER
         */
    }
    
    /**
     * Affecte le logo avec un ensemble de points
     * @param points tableau de points
     */
    public void setPoints(Point [] points) {
        /*
         * A COMPLETER
         */
        this.effacer();
        for (Point p : points){
            getPolygon().addPoint(p.x,p.y);
        }
       
       this.repaint();

    }

    /*
     * A COMPLETER POUR LA GESTION D'EVENEMENT SOURIS
     */        
 
    /**
     * Retourne les points définissant le dessin
     * @return tableau d'entiers
     */
    public Point [] getPoints() {
        Point [] res;
        
        res = new Point[getPolygon().xpoints.length];
        
        for(int i = 0; i < res.length; i++) {
            res[i] = new Point(getPolygon().xpoints[i], getPolygon().ypoints[i]);
        }
        
        return res;
    }
    
    /*
     * Taille fixe
     */
    public Dimension getSize() {
        return new Dimension(150, 150);        
    }    

    @Override
    public void mouseClicked(MouseEvent e) {
        getPolygon().addPoint(e.getX(),e.getY());
        this.repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    /**
     * @return the polygon
     */
    public Polygon getPolygon() {
        return polygon;
    }
}

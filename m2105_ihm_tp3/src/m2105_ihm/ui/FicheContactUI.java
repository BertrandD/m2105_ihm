/*
 * Module 2105 : module IHM : Carnet d'adresse
 */
package m2105_ihm.ui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import m2105_ihm.nf.*;

/**
 *
 * @author IUT2
 */
public class FicheContactUI extends JPanel implements ActionListener{
    
    private Contact contact;
    private JTextField       champNom;
    private JComboBox dispos,region;
    private JTextField champDate,autre,prenom,email,numTel;
    private HashMap<String,JCheckBox> listeHobbies;
    private JCheckBox autreCB;
    private HashMap<String,JCheckBox> listeMusique;
    private JButton bValider;
    private JButton bAnnuler;
    private JButton bSupprimer;

    /**
     * Formulaire pour saisir les informations relatives à un contact
     */
    public FicheContactUI() {
        initUIComponents();
        bValider.addActionListener(this);
        bAnnuler.addActionListener(this);
        bSupprimer.addActionListener(this);
    }
    
    /**
     * Crée et positionne les composants graphiques constituant l'interface
     */
    private void initUIComponents() {      
        /*
         * Ajoute dans l'IHM un champ pour la saisie/Affichage du nom
         */
        this.setLayout(new GridLayout(10,2));
        champNom = new JTextField(30);
        dispos = new JComboBox(DispoSortie.values());
        champDate = new JTextField(10);
        listeHobbies = new HashMap<String,JCheckBox>();
        for(Hobby h:Hobby.values()){
            listeHobbies.put(h.getLabel(),new JCheckBox(h.getLabel()));
        }
        autreCB = new JCheckBox("Autre ");
        autre = new JTextField(10);
        prenom = new JTextField(10);
        email = new JTextField(15);
        numTel = new JTextField(10);
        listeMusique = new HashMap<String,JCheckBox>();
        for(StyleMusical s:StyleMusical.values()){
            listeMusique.put(s.getLabel(),new JCheckBox(s.getLabel()));
        }
        region = new JComboBox(Region.values());
               
        
        /*
         * Ajoute un label associé au champ pour la saisie du nom du contact
         */
        this.add(new JLabel("Nom :"));
        this.add(champNom);

        this.add(new JLabel("Prénom :"));
        this.add(prenom);
        
        this.add(new JLabel("Email :"));
        this.add(email);
        
        this.add(new JLabel("Numéro de téléphone :"));
        this.add(numTel);
        
        this.add(new JLabel("Date de naissance (jj/mm/aaaa):"));
        this.add(champDate);
        
        this.add(new JLabel("Dispos :"));
        this.add(dispos);
        
        this.add(new JLabel("Liste des hobbies :"));
        
        JPanel colHobby = new JPanel();
        colHobby.setLayout(new GridLayout(5,1));
        for(JCheckBox j:listeHobbies.values()){
            colHobby.add(j);
        }

        colHobby.add(autreCB);
        colHobby.add(autre);

        
        this.add(colHobby);

        

        this.add(new JLabel("Styles musicaux :"));

        JPanel col4 = new JPanel();
        col4.setLayout(new GridLayout(listeMusique.size()/3,3));
        for(JCheckBox j:listeMusique.values()){
            col4.add(j);
        }
        this.add(col4);

        this.add(new JLabel("Région :"));
        this.add(region);    
        
        JPanel buttons = new JPanel();
        
        bValider = new JButton("Valider");
        bAnnuler = new JButton("Annuler");
        bSupprimer = new JButton("Supprimer");
        buttons.add(bValider);
        buttons.add(bAnnuler);
        buttons.add(bSupprimer);
        this.add(buttons);
    }
    
    /**
     * Affecte des valeurs au formulaire de fiche contact
     * @param contact un contact
     * @return
     */
    public boolean setValues(Contact contact) {
        boolean success = false;
        this.contact = contact;
        if (contact != null) {
            /*
             * Nom du contact
             */
            champNom.setText(contact.getNom());
            dispos.setSelectedItem(contact.getDisponibilite());
            champDate.setText(
                    contact.getDateNaissance()[2]+"/"+
                    contact.getDateNaissance()[1]+"/"+
                    contact.getDateNaissance()[0]);
            for(Hobby h:contact.getHobbies()){
                listeHobbies.get(h.getLabel()).setSelected(true);
            }
            if(contact.getHobbyAutre().isEmpty()){
                autreCB.setSelected(true);
                autre.setText(contact.getHobbyAutre());
            }
            prenom.setText(contact.getPrenom());
            email.setText(contact.getEmail());
            numTel.setText(contact.getNumeroTelephone());
            for(StyleMusical s:contact.getGoutsMusicaux()){
                listeMusique.get(s.getLabel()).setSelected(true);
            }
            region.setSelectedItem(contact.getRegion());

            success = true;
        }
        
        return success;
    }
    
    /**
     * Retourne les valeurs associées au formulaire de fiche contact
     * @param contact un contact
     * @return
     */
    public boolean getValues(Contact contact) {        
        /*
         * Affecte une valeur à l'attribut Nom avec le nom saisi dans le champ
         * correspondant de l'IHM
         */
        contact.setNom(champNom.getText());
        contact.setPrenom(prenom.getText());
        contact.setEmail(email.getText());
        contact.setNumeroTelephone(numTel.getText());
        contact.setRegion((Region) region.getSelectedItem());
        contact.setDisponibilite((DispoSortie) dispos.getSelectedItem());
        for(JCheckBox h : listeHobbies.values() ){
            if(!h.isSelected()){
                contact.removeHobby(Hobby.valueOf(h.getText()));
            }else{
                contact.addHobby(Hobby.valueOf(h.getText()));
            }
        }
        for(JCheckBox s : listeMusique.values() ){
            if(!s.isSelected()){
                contact.removeStyle(StyleMusical.valueOf(s.getText()));
            }else{
                contact.addStyle(StyleMusical.valueOf(s.getText()));
            }
        }
        if (autreCB.isSelected()){
            contact.setHobbyAutre(autre.getText());
        }
        // TODO :  prendre en compte modif date
        return true;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ConfirmationUI popup = new ConfirmationUI();
        if (e.getSource()==bValider){
            this.getValues(contact);
        }
        else if(e.getSource()==bAnnuler){
            this.setValues(contact);
        }else if(e.getSource()==bSupprimer){
            popup.setVisible(true);
        }
    }
}

/*
 * Module 2105 : module IHM : Carnet d'adresse
 */
package m2105_ihm.ui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import m2105_ihm.nf.Contact;
import m2105_ihm.nf.GroupeContacts;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author IUT2
 */
public class FicheGroupeUI extends javax.swing.JPanel implements ActionListener{   
    /**
     * Creates new form CarnetUI
     */
    private ZoneDessinUI zoneDessin;
    private JTextField champGroupe ;
    private DefaultTableModel tableModel;
    private JButton effacer;
    private JButton valider;
    GroupeContacts groupe;
            
    public FicheGroupeUI() {        
       initUIComponents();
       effacer.addActionListener(this);
       valider.addActionListener(this);
      
    }

    /**
     * Crée et positionne les composants graphiques constituant l'interface
     */    
    private void initUIComponents() {
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        JPanel top = new JPanel(new GridLayout(5,1));
        this.add(top);
        top.add(new JLabel("Fiche groupe"));
        JPanel groupe = new JPanel();
        groupe.add(new JLabel("Nom du Groupe :"));
        champGroupe = new JTextField(10);
        groupe.add(champGroupe);
        top.add(groupe);
        top.add(new JLabel("Liste des membres"));
        
        
        
        String  title[] = {"Nom", "Prenom"};
        tableModel = new DefaultTableModel(title,0);
        
       
        JTable table = new JTable(tableModel);
        JScrollPane listMembre = new JScrollPane(table);
        top.add(listMembre);
        

                
        /*
         * A COMPLETER
         */
        JPanel mid = new JPanel();
        zoneDessin = new ZoneDessinUI();
        
        effacer = new JButton("Effacer le dessin");
        mid.add(zoneDessin);
        mid.add(effacer);
        this.add(mid);
        
        JPanel bot = new JPanel();
        JButton b1 = new JButton("Valider");
        valider = new JButton("Annuler");
        bot.add(b1);
        bot.add(valider);
        this.add(bot);
    }

    /**
     * Affecte des valeurs au formulaire de fiche groupe de contacts
     * @param groupe groupe de contacts
     * @return
     */    
    public boolean setValues(GroupeContacts groupe, Contact [] contacts) {
        boolean success = false;
        this.groupe=groupe;
        if (groupe != null) {
            /*
             * A COMPLETER
             */
            champGroupe.setText(groupe.getNom());
            
            for (Contact c : contacts){
               
                tableModel.addRow(new String[] {c.getNom(),c.getPrenom(),c.getEmail(),c.getNumeroTelephone()});
            }
            
            for (Point p :groupe.getPoints()){
               zoneDessin.getPolygon().addPoint(p.x, p.y);
            }
            
            
            
            
            success = true;
        }
        
        return success;
    }
    
    /**
     * Retourne les valeurs associées au formulaire de fiche groupe de contacts
     * @param groupe
     * @return
     */    
    public boolean getValues(GroupeContacts groupe) {
        if (groupe == null) { return false; }
        
        /*
         * A COMPLETER
         */
        groupe.setNom(champGroupe.getText()) ;
        
        groupe.setPoints(zoneDessin.getPoints());   
        
        /*
         * Ne pas s'occuper des membres du groupe car traité via des
         * commandes du menu qui appeleront setValues
         */
        
        return true;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==effacer){
            zoneDessin.effacer();
        }
        else if(e.getSource()==valider){
            this.getValues(groupe);
        }
       
    }
}

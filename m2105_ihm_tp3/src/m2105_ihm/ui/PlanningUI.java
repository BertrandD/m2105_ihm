/*
 * Module 2105 : module IHM : Carnet d'adresse
 */
package m2105_ihm.ui;

import java.awt.BorderLayout;
import m2105_ihm.Controleur;

/**
 *
 * @author IUT2
 */
public class PlanningUI extends javax.swing.JPanel {
    /**
     * Creates new form CarnetUI
     */
    
    private Controleur controleur;    

    public PlanningUI(Controleur controleur) {
        super();
        
        this.controleur = controleur;
        
        
        initComponents();
    }

    private void initComponents() {
        this.setLayout(new BorderLayout());
        this.add(new javax.swing.JLabel("Liste des Evenements"), BorderLayout.WEST);
    }
}

/*
 * Module 2105 : module IHM : Carnet d'adresse
 */
package m2105_ihm;

import m2105_ihm.ui.CarnetUI;
import m2105_ihm.ui.FenetreUI;
import m2105_ihm.ui.PlanningUI;

import m2105_ihm.nf.Contact;
import m2105_ihm.nf.GroupeContacts;
import m2105_ihm.nf.CarnetPlanning;

/**
 *
 * @author IUT2
 */
public class Controleur {
    
    /*
     * Noyau Fonctionnel
     */    
    CarnetPlanning nf;
            
    /*
     * Composants
     */
    private FenetreUI fenetre;    
    private CarnetUI carnetUI;
    private PlanningUI planningUI;

    /**
     * Constructeur de la fenêtre principale
     */
    public Controleur() {
        nf = new CarnetPlanning();
        
        initUI();
        initContent();
    }
    
    /**
     * Création des composants constituant la fenêtre principale
     */
    private void initUI() {
        fenetre = new FenetreUI(this);
        carnetUI = new CarnetUI(this);                               
        fenetre.addTab(carnetUI, "Carnet");        
        fenetre.afficher();
    }
    
    /**
     * Met à jour la base de données
     */
    public void enregistrer() {
        nf.updateDB();
    }    
        
    /**
     * Quitter l'application sans enregistrer les modifications
     */
    public void quitter() {
        System.exit(0);
    }
    
    /**
     * Action créer un nouveau contact
     */
    public void creerContact() {
        System.out.println("Action pour creer un contact");
    }

    /**
     * Action supprimer contact
     */
    public void supprimerContact() {
        System.out.println("Action pour supprimer un contact");
    }
    
    /**
     * Action créer un groupe de contacts
     */
    public void creerGroupe() {
        System.out.println("Action pour creer un groupe de contacts");
    }

    /**
     * Action supprimer un groupe de contacts
     */
    public void supprimerGroupe() {
        System.out.println("Action pour supprimer un groupe de contacts");
    }
    
    /*
     * Commutation dans les panneaux
     */
    public void tabChanged(int index) {
        switch(index) {
            case 0:
                 fenetre.setEnabled(fenetre.MENU_FICHIER, 0, true);
                 break;
            case 1:
                 fenetre.setEnabled(fenetre.MENU_FICHIER, 0, false);
                 break;                
        }
    }   
    
     /**
     * Indique si un contact ou un groupe est sélectionné
     * @param selected vrai si un contact est sélectionné
     */
    public void setContactSelected(boolean selected) {
        this.contactSelected = selected;
        tabChanged(0);
    }
    
    /**
     * Retourne la liste des contacts associés à un groupe
     */
    public Contact [] getListeContacts(GroupeContacts g) {
        return nf.listerContactsGroupe(g);
    }

    /**
     * Alimente la liste avec la liste des contacts existants
     */
    private void initContent() {
        for(Contact c : nf.listeContacts()) {
            carnetUI.ajouterContact(c.getNom() + " " + c.getPrenom(), c);
        }
        
        for(GroupeContacts g : nf.listeGroupes()) {
            carnetUI.ajouterGroupe(g.getNom(), g);
        }
        carnetUI.showAll();
        carnetUI.selectFirstContact();
        tabChanged(0);
    }
    
    private boolean contactSelected = true;
}
